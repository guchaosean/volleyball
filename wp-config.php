<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'volleyball');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@ 5,8anw){b #|^2ut_yVr&F61)bHGTqd}%1w17&~H*1$#KN1lG?cbkR:spA^pf3');
define('SECURE_AUTH_KEY',  '~^^lPha0@m6EO/8};3FVZmLPHf(Wg_JC)+%M6,V|a9A3M7$QFER-UBZeUuT`11j[');
define('LOGGED_IN_KEY',    '1D@[]}Hlq188M4K~jc!Cu|{Fb,K1P?{k-]Saza|I^D`1g.GI7RkWAHw$a=A6zZP|');
define('NONCE_KEY',        'Q]R6ydlX#BO<tY%l2h19, `c,`}-Rp,V6G*+BfbD+5]sT2qr9aL.(FRgb0pTv#IE');
define('AUTH_SALT',        '_x?^0$cP{U#mAB?@SkezX)P_$4nfzIlAx1-`<]7PKH1mT=XmrUYa+//3qW`iB#Hg');
define('SECURE_AUTH_SALT', 'o+>UdK_9BEdPP`)@<3gV]hN)XAu|~76]2Xu @CAZ>DzYTgk,e_+Cz/*jj*{G&:3f');
define('LOGGED_IN_SALT',   ' 1nqBEg6g+%`^o/Vz7E:AOuiTW.-e9%e]th[Tr?BVI:ky}vE)-Gbz)h>b:>/I]1B');
define('NONCE_SALT',       'Z[]@N#VgT8G+.inskC*!iobze7EWbNJrgAP,MD)f+gCCd{uZusOjKewOH6*JHugu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
