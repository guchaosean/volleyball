<?php
/**
 * Template Name: Blog Template
 */
?>
<div class="blogs">
    <div class="container">
        <h1>RECENT NEWS</h1>
        <?php
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;

        $custom_args = array(
            'post_type' => 'post',
            'posts_per_page' => 6,
            'paged' => $paged
        );

        $custom_query = new WP_Query($custom_args);
        $index = 0;
        $count_posts = wp_count_posts();
        ?>

        <?php if ($custom_query->have_posts()) : ?>

            <!-- the loop -->
            <?php while ($custom_query->have_posts()) : $custom_query->the_post(); ?>
                <?php
                $index++;
                if ($index % 3 == 1) {
                    ?>
                    <div class="row blog recent_news">
                        <?php
                    }
                    ?>
                    <div class="col-md-4 clo-sm-12">
                        <div class="image" style="background-image: url('<?= get_the_post_thumbnail_url(); ?>')">
                        </div>
                        <div class="postcontent">
                            <h4><?php the_title(); ?></h4>

                            <?php the_excerpt(); ?>


                            <a href="<?= get_permalink() ?>"><span>read more..</span></a>

                        </div>
                    </div>    

                    <?php
                    if (($index % 3 == 0)) {
                        ?>                        
                    </div>
                    <?php
                }
                ?>


            <?php endwhile; ?>
            <?php
            if ($index % 3 != 0) {
                ?>
            </div>
            <?php
        }
        ?> 

        <!-- end of the loop -->

        <!-- pagination here -->
        <?php
        if (function_exists(custom_pagination)) {
            custom_pagination($custom_query->max_num_pages, "", $paged);
        }
        ?>

        <?php wp_reset_postdata(); ?>

    <?php else: ?>
        <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
</div>
</div>

<style>
    .postcontent,   .postcontent a{
        background-color: #f0f0f0;
        color:black;
    }
    .postcontent{
        height: 62%;
    }
    .custom-pagination{
        margin-top: 50px;
        margin-bottom: 50px;
    }
</style>







