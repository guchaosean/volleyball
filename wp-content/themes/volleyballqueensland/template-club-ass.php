<?php
/**
 * Template Name: Club and Association Template
 */
?>
<div class="container club-ass">
    <H2>VQ CLUBS</H2>

    Volleyball Queensland has various affiliated clubs throughout Queensland, delivering volleyball programs for junior through to senior players. Affiliated clubs are grouped in three regions; North Queensland, Central Queensland and South Queensland .
    <br><br>
    If you're looking to become a member of the volleyball community, then look up the nearest club to you and get in contact. Click on the name of the club below to be directed to their website, or email to find out more information on that club.


    <?php
    $catergorys = get_terms(array(
        'taxonomy' => 'wpsl_store_category',
        'hide_empty' => false,
    ));
    foreach ($catergorys as $catergory) {
        if ($catergory->count > 0) {
            ?>
            <h2><?= $catergory->name ?></h2>
            <?php
            $args = array(
                'post_type' => 'wpsl_stores',
                'order' => 'ASC',
                'orderby' => 'name',
                'posts_per_page' => 100,
                'tax_query' => [
                    'relation' => 'AND',
                    [
                        'taxonomy' => 'wpsl_store_category',
                        'field' => 'id',
                        'terms' => $catergory->term_id,
                        
                    ]
                ]
            );

            $locations = get_posts($args);
            $index = 0;
            foreach ($locations as $location) {

                $meta = get_post_meta($location->ID);
                //$cat = get_the_terms($location, 'wpsl_store_category');
            //  if ($cat[0]->term_id == $catergory->term_id) {
                    $index++;
                   
                    if ($index % 2 == 1) {
                        ?>
                        <div class="row">
                        <?php } ?>

                        <div class="col-md-1 col-sm-2 block">
                            <img class="club-img" src="<?= get_the_post_thumbnail_url($location) ?>"></img> 
                        </div>   

                        <div class="col-md-5 col-sm-10">
                            <h4><?= $location->post_title ?></h4>
                            <p>Location: <?= $meta['wpsl_city'][0] ?></p>
                            <?php
                            if (isset($meta['wpsl_phone'])) {
                                ?>
                                <p>Phone: <?= $meta['wpsl_phone'][0] ?></p>
                                <?php
                            }
                            ?>
                            <?php
                            if (isset($meta['wpsl_description'])) {
                                ?>
                                <p>Info: <?= $meta['wpsl_description'][0] ?></p>
                                <?php
                            }
                            ?>

                        </div>   

                        <?php if (($index % 2 == 0) || ($index == sizeof($locations))) { ?>
                        </div>
                    <?php } ?>
                    <hr>
                    <?php
               // }
            }
            ?>


            <?php
        }
    }
    ?>



</div>

