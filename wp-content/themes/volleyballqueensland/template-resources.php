<?php
/**
 * Template Name: Resources Template
 */
?>

<div class="container" style="padding-bottom:30px;">
    <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/page', 'header'); ?>
        <?php get_template_part('templates/content', 'page'); ?>
    <?php endwhile; ?>


    <?php
    $left = [];
    $right = [];
    $output = "";
    ?>

    <?php
    $terms = get_terms('category_media', array(
        'hide_empty' => false,
    ));
    $index = 1;
    foreach ($terms as $term) {

        $COUNT = $term->count;
        if ($COUNT > 0) {
            if ($index % 2 == 1) {
                array_push($left, $term);
            } else {
                array_push($right, $term);
            }
            $index++;
        }
    }
    ?>

    <?php
    $output = $output . '<div class="row">';
    $output = $output . '[su_accordion class="col-6"]';
    foreach ($left as $term) {
        $name = $term->name;
        $output = $output . '[su_spoiler title="' . $name . '" open="no" style="default" icon="plus" anchor="" class=""]';
        $slug = $term->slug;
        $query_images_args = array(
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category_media',
                    'field' => 'slug',
                    'terms' => $slug,
                ),
            ),
        );
        $query_images = new WP_Query($query_images_args);
        $output = $output . ' <div class="resource-body">';
        while ($query_images->have_posts()) {
            $query_images->the_post();

            $name = $post->post_title;
            $url = $post->guid;


            $output = $output . '<p><a href="' . $url . '">' . $name . '</a></p>';
        }

        $output = $output . '</div>';
        $output = $output . '[/su_spoiler]';
    }
    $output = $output . '[/su_accordion]';

    $output = $output . '[su_accordion class="col-6"]';
    foreach ($right as $term) {
        $name = $term->name;
        $output = $output . '[su_spoiler title="' . $name . '" open="no" style="default" icon="plus" anchor="" class=""]';
        $slug = $term->slug;
        $query_images_args = array(
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'tax_query' => array(
                array(
                    'taxonomy' => 'category_media',
                    'field' => 'slug',
                    'terms' => $slug,
                ),
            ),
        );
        $query_images = new WP_Query($query_images_args);
        $output = $output . ' <div class="resource-body">';
        while ($query_images->have_posts()) {
            $query_images->the_post();

            $name = $post->post_title;
            $url = $post->guid;


            $output = $output . '<p><a href="' . $url . '">' . $name . '</a></p>';
        }

        $output = $output . '</div>';
        $output = $output . '[/su_spoiler]';
    }
    $output = $output . '[/su_accordion]';
    ?>


    <?php $output = $output . '</div>'; ?>


    <?php echo do_shortcode($output); ?>  

</div>
