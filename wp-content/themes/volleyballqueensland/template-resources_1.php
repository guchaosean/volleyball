<?php
/**
 * Template Name: Resources Template
 */
?>
<div class="container">
    <?php while (have_posts()) : the_post(); ?>
        <?php get_template_part('templates/page', 'header'); ?>
        <?php get_template_part('templates/content', 'page'); ?>
    <?php endwhile; ?>

    <?php
    $terms = get_terms('category_media', array(
        'hide_empty' => false,
    ));
    $index = 1;
    foreach ($terms as $term) {
        $name = $term->name;
        $COUNT = $term->count;
        if ($COUNT > 0) {
            if ($index % 2 == 1) {
                ?>
                <div class="row">
                    <?php
                }
                ?>
                <div class="resource-panel col-6">
                    <div class="resource-header">
                        <h3  ><?= $name ?></h3>
                    </div>
                    <?php
                    $slug = $term->slug;
                    $query_images_args = array(
                        'post_type' => 'attachment',
                        'post_status' => 'inherit',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category_media',
                                'field' => 'slug',
                                'terms' => $slug,
                            ),
                        ),
                    );
                    $query_images = new WP_Query($query_images_args);
                    ?>
                    <div class="resource-body">
                        <?php
                        while ($query_images->have_posts()) {
                            $query_images->the_post();
                           
                            $name = $post->post_title;
                            $url = $post->guid;
                            ?>
                        
                            <p><a href="<?=  $url ?>"><?= $name ?></a></p>
                            <?php
                        }
                        if ($index % 2 == 0) {
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
            $index++;
        }
    }
    ?>
</div>
