<?php
/**
 * Event Blocks
 *
 * @author 		ThemeBoy
 * @package 	SportsPress/Templates
 * @version     2.2.6
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$defaults = array(
    'id' => null,
    'title' => false,
    'status' => 'default',
    'date' => 'default',
    'date_from' => 'default',
    'date_to' => 'default',
    'day' => 'default',
    'league' => null,
    'season' => null,
    'venue' => null,
    'team' => null,
    'player' => null,
    'number' => -1,
    'show_team_logo' => get_option('sportspress_event_blocks_show_logos', 'yes') == 'yes' ? true : false,
    'link_teams' => get_option('sportspress_link_teams', 'no') == 'yes' ? true : false,
    'link_events' => get_option('sportspress_link_events', 'yes') == 'yes' ? true : false,
    'paginated' => get_option('sportspress_event_blocks_paginated', 'yes') == 'yes' ? true : false,
    'rows' => get_option('sportspress_event_blocks_rows', 5),
    'orderby' => 'default',
    'order' => 'default',
    'show_all_events_link' => false,
    'show_title' => get_option('sportspress_event_blocks_show_title', 'no') == 'yes' ? true : false,
    'show_league' => get_option('sportspress_event_blocks_show_league', 'no') == 'yes' ? true : false,
    'show_season' => get_option('sportspress_event_blocks_show_season', 'no') == 'yes' ? true : false,
    'show_venue' => get_option('sportspress_event_blocks_show_venue', 'no') == 'yes' ? true : false,
    'hide_if_empty' => false,
);

extract($defaults, EXTR_SKIP);

$calendar = new SP_Calendar($id);
if ($status != 'default')
    $calendar->status = $status;
if ($date != 'default')
    $calendar->date = $date;
if ($date_from != 'default')
    $calendar->from = $date_from;
if ($date_to != 'default')
    $calendar->to = $date_to;
if ($league)
    $calendar->league = $league;
if ($season)
    $calendar->season = $season;
if ($venue)
    $calendar->venue = $venue;
if ($team)
    $calendar->team = $team;
if ($player)
    $calendar->player = $player;
if ($order != 'default')
    $calendar->order = $order;
if ($orderby != 'default')
    $calendar->orderby = $orderby;
if ($day != 'default')
    $calendar->day = $day;
$data = $calendar->data();

if ($hide_if_empty && empty($data))
    return false;

if ($show_title && false === $title && $id):
    $caption = $calendar->caption;
    if ($caption)
        $title = $caption;
    else
        $title = get_the_title($id);
endif;

if ($title)
    echo '<h4 class="sp-table-caption">' . $title . '</h4>';
?>
<div class="sp-template sp-template-event-blocks">

    <?php
    $availableRound[] = "";
    foreach ($data as $event) {
        if (strtotime($event->post_date) > strtotime("now")) {
            $eventID = $event->ID;
            $nextRound = get_field('round', $eventID);
            break;
        }
        $lastEvent = $event;
    }
    if (!isset($nextRound)) {
        $eventID = $lastEvent->ID;
        $nextRound = get_field('round', $eventID);
    }
    ?>
    <script>
        var nextRound = '<?= $nextRound ?>';
    </script>

    <?php
    foreach ($data as $event) {

        $eventID = $event->ID;
        $round = get_field('round', $eventID);
        if (isset($availableRound[$round])) {
            $availableRound[$round] ++;
        } else {
            $availableRound[$round] = 1;
        }
    }
    unset($availableRound[0]);
    ?>
    <ul class="sp-event-block-round-filter">      
        <?
        foreach ($availableRound as $round => $value) {
            if (preg_match("/[a-z]/", $round)) {
                $roundArray = explode('-', $round);
                if (in_array('semi', $roundArray)) {
                    ?>
                    <li><a class="event-sroll"  data-round="<?= $round ?>">SF<p><?= $roundArray[1] ?></p></a></li>
                    <?php
                }
                if (in_array('pre', $roundArray)) {
                    ?>
                    <li><a class="event-sroll"  data-round="<?= $round ?>">PF<p><?= $roundArray[1] ?></p></a></li>
                    <?php
                }
                if (in_array('final', $roundArray)) {
                    ?>
                    <li><a class="event-sroll"  data-round="<?= $round ?>">Final<p style="text-transform:uppercase!important"><?= $roundArray[1][0] ?></p></a></li>
                    <?php
                }
            } else {
                ?>
                <li><a class="event-sroll"  data-round="<?= $round ?>">Round<p><?= $round ?></p></a></li>
                <?php
            }
        }
        ?>
    </ul>
    <?php
    if (isset($_GET['round']) && ($_GET['round'] !== 'all')) {
        foreach ($data as $key => $event) {
            $eventID = $event->ID;
            $round = get_field('round', $eventID);
            if ($round !== $_GET['round']) {
                unset($data[$key]);
            }
        }
    }
    ?>

    <div class="sp-table-wrapper">
        <?php
        if (sizeof($data) == 0) {
            echo "<h3>There is no match for this Round.</h3>";
        } else {
            ?>    

            <table class="sp-event-blocks sp-data-table<?php if ($paginated) { ?> sp-paginated-table<?php } ?>" data-sp-rows="<?php echo $rows; ?>">
                <thead><tr><th></th></tr></thead> <?php # Required for DataTables                                                                                                                                                                   ?>
                <tbody>
                    <?php
                    $i = 0;
                    if (intval($number) > 0)
                        $limit = $number;
                    $roundCheck[] = false;
                    foreach ($data as $event):
                        if (isset($limit) && $i >= $limit)
                            continue;

                        $permalink = get_post_permalink($event, false, true);
                        $results = get_post_meta($event->ID, 'sp_results', true);

                        $teams = array_unique(get_post_meta($event->ID, 'sp_team'));
                        $teams = array_filter($teams, 'sp_filter_positive');
                        $logos = array();

                        if ($show_team_logo):
                            $j = 0;
                            foreach ($teams as $team):
                                $teamName = get_the_title($team);
                                $j++;
                                if (has_post_thumbnail($team)):
                                    if ($link_teams) {
                                        $logo = '<a class="team-logo logo-' . ( $j % 2 ? 'odd' : 'even' ) . '" href="' . get_permalink($team, false, true) . '" title="' . get_the_title($team) . '">' . get_the_post_thumbnail($team, 'sportspress-fit-icon') . '</a>';
                                    } else {
                                        if ($j % 2) {
                                            $logo = '<div class="team-logo logo-' . ( $j % 2 ? 'odd' : 'even' ) . '" title="' . get_the_title($team) . '">' . get_the_post_thumbnail($team, 'sportspress-fit-icon') . "<span class='sp-team-name'>" . $teamName . "</span>" . '</div>';
                                        } else {
                                            $logo = '<div class="team-logo logo-' . ( $j % 2 ? 'odd' : 'even' ) . '" title="' . get_the_title($team) . '">' . "<span class='sp-team-name'>" . $teamName . "</span>" . get_the_post_thumbnail($team, 'sportspress-fit-icon') . '</div>';
                                        }
                                    }

                                    $logos[] = $logo;
                                endif;
                            endforeach;
                        endif;

                        if ('day' === $calendar->orderby):
                            $event_group = get_post_meta($event->ID, 'sp_day', true);
                            if (!isset($group) || $event_group !== $group):
                                $group = $event_group;
                                echo '<tr><th><strong class="sp-event-group-name">', __('Match Day', 'sportspress'), ' ', $group, '</strong></th></tr>';
                            endif;
                        endif;
                        ?>

                        <tr class="sp-row sp-post<?php echo ( $i % 2 == 0 ? ' alternate' : '' ); ?>">

                            <td>

                                <div  class="sp-event-block ">
                                    <?php
                                    $eventID = $event->ID;
                                    $round = get_field('round', $eventID);

                                    if ($roundCheck[$round] == false) {
                                        if (preg_match("/[a-z]/", $round)) {
                                            $roundArray = explode('-', $round);
                                            if (in_array('semi', $roundArray)) {
                                                echo "<h3 class='round-caption' id='sp-event-round-" . $round . "'>Semi Final " . $roundArray[1] . " </h3>";
                                            }
                                            if (in_array('pre', $roundArray)) {
                                                echo "<h3 class='round-caption' id='sp-event-round-" . $round . "'>Preliminary Final " . $roundArray[1] . " </h3>";
                                            }
                                            if (in_array('final', $roundArray)) {
                                                echo "<h3 class='round-caption' id='sp-event-round-" . $round . "'>Final " . $roundArray[1] . " </h3>";
                                            }
                                        } else {
                                            echo "<h3 class='round-caption' id='sp-event-round-" . $round . "'>round " . $round . " </h3>";
                                        }
                                    }
                                    $roundCheck[$round] = true;
                                    ?>
                                    <div class="sp-event-block-header">
                                        <?php
                                        if ($show_venue): $venues = get_the_terms($event, 'sp_venue');
                                            if ($venues): $venue = array_shift($venues);
                                                ?>
                                                <div class="sp-event-venue"><?php echo $venue->name; ?></div>
                                                <?php
                                            endif;
                                        endif;
                                        ?>
                                        <time class="sp-event-date" datetime="<?php echo $event->post_date; ?>">
                                            <?php echo sp_add_link(get_the_time(get_option('date_format'), $event), $permalink, $link_events); ?>
                                        </time>
                                        <?php
                                        if ($show_league): $leagues = get_the_terms($event, 'sp_league');
                                            if ($leagues): $league = array_shift($leagues);
                                                ?>

                                                <?php
                                            endif;
                                        endif;
                                        ?>
                                        <?php
                                        if ($show_season): $seasons = get_the_terms($event, 'sp_season');
                                            if ($seasons): $season = array_shift($seasons);
                                                ?>
                                                <div class="sp-event-league"><?php echo $league->name . " " . $season->name; ?></div>

                                                <?php
                                            endif;
                                        endif;
                                        ?>
                                    </div>
                                    <div class="sp-event-block-content">
                                        <?php echo implode($logos, ' '); ?>
                                        <h5 class="sp-event-results">
                                            <?php echo sp_add_link('<span class="sp-result">' . implode('</span> - <span class="sp-result">', apply_filters('sportspress_event_blocks_team_result_or_time', sp_get_main_results_or_time($event), $event->ID)) . '</span>', $permalink, $link_events); ?>
                                        </h5>
                                        <p class="sp-event-results-details">
                                            <?php
                                            $eventTemp = new SP_Event($event->ID);
                                            $data = $eventTemp->results();
                                            unset($data[0]);
                                            $teamIndex = 1;
                                            foreach ($data as $team_id => $result) {
                                                unset($result['outcome']);
                                                unset($result['points']);
                                                $quaterIndex = 1;
                                                foreach ($result as $quater => $score) {
                                                    if ($score !== ' ') {
                                                        $teamData[$teamIndex][$quaterIndex] = $score;
                                                    }
                                                    $quaterIndex++;
                                                }
                                                $teamIndex++;
                                            }
                                            for ($i = 1; $i < $quaterIndex; $i++) {
                                                if (strlen($teamData[1][$i]) != 0) {
                                                    echo $teamData[1][$i] . "-" . $teamData[2][$i] . ' ';
                                                }
                                            }
                                            ?>
                                        </p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $i++;
                    endforeach;
                    ?>
                </tbody>
            </table>
            <?php
        }
        ?>

    </div>
    <?php
    if ($id && $show_all_events_link)
        echo '<div class="sp-calendar-link sp-view-all-link"><a href="' . get_permalink($id) . '">' . __('View all events', 'sportspress') . '</a></div>';
    ?>
</div>