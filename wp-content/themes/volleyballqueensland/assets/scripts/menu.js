/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $(document).ready(function () {
        $('.carousel').carousel();
    });

//    $(window).on('load', function () {
//        if (typeof nextRound !== 'undefined') {
//            $("html, body").animate({scrollTop: $('#sp-event-round-' + nextRound).offset().top - 200}, 300);
//        }
//    });


//    $('.navigation>li').hover(function () {
//        if ($(window).width() >= 768) {
//            $(this).find('.megamenu').css('display', 'flex');
//            $(this).find('.sub_menu').show();
//        }
//
//    }, function () {
//        if ($(window).width() >= 768) {
//            $(this).find('.megamenu').css('display', 'none');
//            $(this).find('.sub_menu').hide();
//        }
//    });

    $('.navigation>li').click(function () {

        if ($(window).width() >= 768) {
            if ($(this).hasClass('focus')) {
                $(this).removeClass('focus');
                $('.sub_menu').hide();
                $('.megamenu').css('display', 'none');
            } else {
                $('.sub_menu').hide();
                $('.megamenu').css('display', 'none');
                $('.navigation>li').removeClass('focus');
                $(this).addClass('focus');
                $(this).find('.megamenu').css('display', 'flex');
                $(this).find('.sub_menu').show();
            }

        }

    });

    $('.banner').click(function () {
        console.log("click");
    });


    $('.tooglemenu').click(function () {
        if ($('.menu_section').hasClass('closed')) {
            $('.menu_section').removeClass('closed');
            $('.menu_section').addClass('opened');
        } else {
            $('.menu_section').removeClass('opened');
            $('.menu_section').addClass('closed');
        }
    });


    $('.navigation>li').click(function () {
        if ($(window).width() <= 768) {
            if ($(this).find('.sub_menu').length !== 0) {
                if ($(this).find('.sub_menu').hasClass('closed')) {
                    $('.navigation>li').find('.opened').addClass('closed');
                    $('.navigation>li').find('.opened').removeClass('opened');
                    $(this).find('.sub_menu').removeClass('closed');
                    $(this).find('.sub_menu').addClass('opened');
                } else {
                    $(this).find('.sub_menu').removeClass('opened');
                    $(this).find('.sub_menu').addClass('closed');
                }
            }
        }
    });

    $('.navigation>li').click(function () {
        if ($(window).width() <= 768) {
            if ($(this).find('.megamenu').length !== 0) {
                if ($(this).find('.megamenu').hasClass('closed')) {
                    $('.navigation>li').find('.opened').addClass('closed');
                    $('.navigation>li').find('.opened').removeClass('opened');
                    $(this).find('.megamenu').removeClass('closed');
                    $(this).find('.megamenu').addClass('opened');
                } else {
                    $(this).find('.megamenu').removeClass('opened');
                    $(this).find('.megamenu').addClass('closed');
                }
            }
        }
    });

    $('.tooglemenu').click(function () {
        if ($(window).width() <= 768) {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        }
    });

    $('.event-sroll').click(function () {
        var round = $(this).data('round');
        console.log($('#sp-event-round-' + round).offset());

        $("html, body").animate({scrollTop: $('#sp-event-round-' + round).offset().top - 200}, 300);
    });

    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop >= 552) {
            $('.sp-event-block-round-filter').addClass('round-filter-static');
            $('#pre-roundlist').addClass('hiden');
            $('#nxt-roundlist').addClass('hiden');
        } else {
            $('.sp-event-block-round-filter').removeClass('round-filter-static');
            $('#pre-roundlist').removeClass('hiden');
            $('#nxt-roundlist').removeClass('hiden');
        }
    });

    $('#nxt-roundlist').on('click', function () {
        var $first = $('.sp-event-block-round-filter li:first');
        $first.animate({'margin-left': '-30px'}, 200, function () {
            $first.remove().css({'margin-left': '0px'});
            $('.sp-event-block-round-filter li:last').after($first);
        });
    });

    $('#pre-roundlist').on('click', function () {
        var $last = $('.sp-event-block-round-filter li:last');
        $last.remove().css({'margin-left': '-30px'});
        $('.sp-event-block-round-filter li:first').before($last);
        $last.animate({'margin-left': '0px'}, 400);
    });

    $('.attri-filter-list>li').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.filter-list-common').hide();
            $('.filter-list-common').css('opacity', 0);
        } else {
            var attr = $(this).data('att');
            $('.attri-filter-list>li').removeClass('active');
            $(this).addClass('active');
            $('.filter-list-common').css('opacity', 0);
            $('.filter-list-common').hide();
            $('.' + attr + '-filter').css('opacity', 1);
            $('.' + attr + '-filter').show();
        }

    });
    //Division filter for event list
    $('.sp-template-event-list>.division-filter>li').click(function () {
        var searchName = $(this).html();
        if (searchName !== 'All Division') {
            $(".sp-event-list-item").hide();
            $(".sp-event-list-item")
                    .filter(function (index) {
                        return $(this).children('.sp-event-list-sets-division').data('division') === searchName;
                    })
                    .show();
        } else {
            $(".sp-event-list-item").show();
        }
        if (!$(this).hasClass('active')) {
            $('.sp-template-event-list>.division-filter>li').removeClass('active');
            $(this).addClass('active');
        }
    });
    //Division filter for event block
    $('.sp-template-event-blocks>.division-filter>li').click(function () {
        var searchName = $(this).html();
        if (searchName !== 'All Division') {
            $(".sp-post").css('display', 'none');
            $(".sp-post")
                    .filter(function (index) {
                        return $(this).data('division') === searchName;
                    })
                    .css('display', 'table-row');
        } else {
            $(".sp-post").css('display', 'table-row');
        }
        if (!$(this).hasClass('active')) {
            $('.sp-template-event-blocks>.division-filter>li').removeClass('active');
            $(this).addClass('active');
        }
    });

    $('.grid').drystone({
        xs: [576, 1],
        sm: [768, 2],
        md: [992, 2],
        lg: [1200, 2],
        xl: 2,
    });



    $("ul.nav-tabs > li > a").on("click", function (e) {
        var href = $(this).data('ref');
        var buttons = $('.button_group').find('a');

        buttons.each(function (index) {
            if (!$(this).hasClass('all')) {
                var ehref = $(this).attr('href');
                var n = ehref.indexOf("tab");
                if (n > 0) {
                    var left = ehref.substring(0, n);
                    $(this).attr('href', left + 'tab=' + href);
                } else {
                    $(this).attr('href', ehref + '&tab=' + href);
                }
            }
        });

    });




})(jQuery); // Fully reference jQuery after this point.
