<?php
/**
 * Template Name: Event Template
 */
$id = get_the_ID();
$event = new SP_Event($id);


$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id())[0];
?>
<div class="event-template">
    <div class="container main-container" >
        <div class="background-container event-block" style='background-image:url("<?= $thumbnail ?>")'>
            <div class="container">
                <h1>
                    Match Center 
                </h1>
                <?= do_shortcode('[event_blocks id="' . $id . '"]'); ?>
            </div>
        </div>

        <div class="event-statistics event-block">
            <h2>
                MATCH STATISTICS BY TEAMS
            </h2>
            <hr>
            <?= do_shortcode('[event_performance  id="' . $id . '"]'); ?>
        </div>

        <div class="player-statistics event-block">
            <h2>
                MATCH STATISTICS BY PLAYERS
            </h2>
            <hr>
            <?php
            //get Arguement
            $sections = get_option('sportspress_event_performance_sections', -1);
            $show_players = get_option('sportspress_event_show_players', 'yes') === 'yes' ? true : false;
            $show_staff = get_option('sportspress_event_show_staff', 'yes') === 'yes' ? true : false;
            $show_total = get_option('sportspress_event_show_total', 'yes') === 'yes' ? true : false;
            $teams = get_post_meta($id, 'sp_team', false);

            $id = get_the_ID();
            $event = new SP_Event($id);
            $performance = $event->performance();
            //Get labels
            $labels = $performance[0];
            unset($performance[0]);

            foreach ($teams as $index => $team_id) {
                if (-1 == $team_id)
                    continue;

                // Get results for players in the team
                $players = sp_array_between((array) get_post_meta($id, 'sp_player', false), 0, $index);

                $has_players = sizeof($players) > 1;

                $players = apply_filters('sportspress_event_performance_split_team_players', $players);

                $show_team_players = $show_players && $has_players;

                if (!$show_team_players && !$show_staff && !$show_total)
                    continue;

                if ($show_team_players || $show_total) {
                    if (0 < $team_id) {
                        $data = sp_array_combine($players, sp_array_value($performance, $team_id, array()));
                    } elseif (0 == $team_id) {
                        $data = array();
                        foreach ($players as $player_id) {
                            if (isset($performance[$player_id][$player_id])) {
                                $data[$player_id] = $performance[$player_id][$player_id];
                            }
                        }
                    } else {
                        $data = sp_array_value(array_values($performance), $index);
                    }
                    unset($data[0]);
                    $lineups = array_filter($data, array($event, 'lineup_filter'));
                    $subs = array_filter($data, array($event, 'sub_filter'));

                    var_dump($lineups);
                    var_dump($subs);
                }
            }
            ?>
        </div>



    </div>
</div>