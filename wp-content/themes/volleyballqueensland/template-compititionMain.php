<?php
/**
 * Template Name: Main Competition Template
 */
?>
<div class="competition-main">
    <div class="container">
        <h1>league</h1>
        <?php
        get_template_part('templates/league-calendar');
        ?>

    </div>

</div>    

<div class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 introbox">
                <h2>welcome to</h2>
                <h3>volleyball queensland</h3>
                <p><?php
                    echo get_field('welcome_text', 'options');
                    ?></p>
                <a href="/about/about-volleyball-qld/"><button class="skyblue_button">read more</button></a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 introbox themap">
                <img class="map_image" src="<?= get_field('map_image', 'options') ?>"></img>
            </div>
            <?php
            get_template_part('templates/location-finder');
            ?>
        </div>
    </div>
</div>