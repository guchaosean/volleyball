<?php
/**
 * Event List
 *
 * @author 		ThemeBoy
 * @package 	SportsPress/Templates
 * @version     2.2
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$calendar = new SP_Calendar($id);

$leagu = get_the_terms($id, 'sp_league');
$season = get_the_terms($id, 'sp_season');


$cat = 'sp_league_' . $leagu[0]->term_id;
$sex = get_field('competition_man_or_woman', $cat);
$data = $calendar->data();
?>

<div class="sp-template sp-template-event-list sp-template-league-table">
    <ul class="division-filter ">
        <li class="active">All Division</li>
        <?php
        foreach ($leagu as $division) {
            ?>
            <li><?= $division->name ?></li>
            <?php
        }
        ?> 
        <div class="clearfix"></div>
    </ul>

    <ul class="attri-filter-list">
        <!--<li data-att="division">Division</li>-->
        <li data-att="season">Season</li>

    </ul>



    <ul class="season-filter filter-list-common">
        <li>2017</li>
        <li>2018</li>
        <div class="clearfix"></div>
    </ul>



    <!--<table class="table table-striped sp-league-table sp-event-list-table sp-event-list-<?= $sex ?>">-->
    <table class="table table-striped sp-league-table sp-event-list-table  ">
        <tr class="doubleheader">
            <th class="result-details-small">
                Game</th>
            <th class="result-details">
                Round</th>
            <th class="result-details">
                Date</th>
            <th class="result-details">
                Time</th>
            <th class="result-details">
                Division</th>
            <th >
                Teams</th>
            <th >
                Sets</th>
            <th class="points">1</th>
            <th class="points">2</th>
            <th class="points">3</th>
            <th class="points">4</th>
            <th class="points">5</th>
            <th class="points">Points</th>

        </tr>

        <?php
        foreach ($data as $key => $eventMeta) {

            $date = date('d/m/Y', strtotime($eventMeta->post_date));
            $time = date('h:i A', strtotime($eventMeta->post_date));
            $event = new SP_Event($eventMeta);
            $results = $event->results();
            unset($results[0]);
            $keys = array_keys($results);
            $team1 = new SP_Team($keys[0]);
            $team2 = new SP_Team($keys[1]);

            $leagu = get_the_terms($eventMeta->ID, 'sp_league');
            $cat = 'sp_league_' . $leagu[0]->term_id;
            $sex = get_field('competition_man_or_woman', $cat);
            ?>
            <tr class="sp-event-list-item sp-event-list-<?= $sex ?>">
    <!--                <td class="sp-event-list-number"><?= $key + 1 ?></td>-->
                <td class="result-details"><?php
                    $Currentround = get_field('round', $eventMeta->ID);
                    if (preg_match("/[a-z]/", $Currentround)) {
                        $roundArray = explode('-', $Currentround);
                        if (in_array('semi', $roundArray)) {
                            $roundText = "SF" . $roundArray[1] . " </h3>";
                            $roundTextSmall = "SF" . $roundArray[1] . " </h3>";
                            echo $roundText;
                        }
                        if (in_array('pre', $roundArray)) {
                            $roundText = "PF" . $roundArray[1] . " </h3>";
                            $roundTextSmall = "PF" . $roundArray[1] . " </h3>";
                            echo $roundText;
                        }
                        if (in_array('final', $roundArray)) {
                            $roundText = $roundArray[1] . " </h3>";
                            $roundTextSmall = $roundArray[1] . " </h3>";
                            echo $roundText;
                        }
                    } else {
                        $roundText = $Currentround;
                        $roundTextSmall = "Round " . $Currentround;
                        echo $roundText;
                    }
                    ?></td>
                <td class="result-details"><p><?= $date ?></p> </td>
                <td class="result-details"><?= $time ?></td>

                <?php
                $leagu = get_the_terms($eventMeta->ID, 'sp_league');
                $name = $leagu[0]->name;
                $tempstring1 = explode('(', $name);
                $tempstring2 = explode(')', $tempstring1[1]);
                ?>
                <td class="sp-event-list-sets-division result-details" data-division="<?= $name ?>"><?= $tempstring2[0] ?></td>
                <td class="result-details-small">
                    <p><?= $roundTextSmall . " - " . $tempstring2[0] ?></p>
                    <p class="vs"><?= $date ?></p>
                    <p><?= $time ?></p>
                </td>
                <td class="sp-event-list-team-name result-details"><?php
                    //$teamresult1 = $results[$keys[0]]['one'] + $results[$keys[0]]['two'] + $results[$keys[0]]['three'] + $results[$keys[0]]['four'] + $results[$keys[0]]['five'];
                    //$teamresult2 = $results[$keys[1]]['one'] + $results[$keys[1]]['two'] + $results[$keys[1]]['three'] + $results[$keys[1]]['four'] + $results[$keys[1]]['five'];
                    $teamresult1 = $results[$keys[0]]['points'];
                    $teamresult2 = $results[$keys[1]]['points'];
                    if (sizeof($results[$keys[0]]) !== 0) {
                        if ($teamresult1 > $teamresult2) {
                            echo "<span class='sp-result-won'>" . $team1->post->post_title . "</span>" . " - " . $team2->post->post_title;
                        } else {
                            echo $team1->post->post_title . " - " . "<span class='sp-result-won'>" . $team2->post->post_title . "</span>";
                        }
                    } else {
                        echo $eventMeta->post_title;
                    }
                    ?></td>
                <td class="sp-event-list-team-name result-details-small"><?php
                    $teamresult1 = $results[$keys[0]]['points'];
                    $teamresult2 = $results[$keys[1]]['points'];
                    if (sizeof($results[$keys[0]]) !== 0) {
                        if ($teamresult1 > $teamresult2) {
                            echo "<p class='sp-result-won team'>" . $team1->post->post_title . "</p>" . "<p class='vs'> vs </p>" . "<p class='team'>" . $team2->post->post_title . "</p>";
                        } else {
                            echo "<p class='team' >" . $team1->post->post_title . "</p>" . "<p class='vs'> vs </p>" . "<p class='sp-result-won team'>" . $team2->post->post_title . "</p>";
                        }
                    } else {
                        echo $eventMeta->post_title;
                    }
                    ?></td>

                <td class="sp-event-list-sets-result result-details"><?= $results[$keys[0]]['points'] . " - " . $results[$keys[1]]['points'] ?></td>
                <td class="sp-event-list-sets-result result-details-small">
                    <?= "<p class='score'>" . $results[$keys[0]]['points'] . "</p>" . "<p class='vs'> - </p>" . "<p class='score'>" . $results[$keys[1]]['points'] . "</p>" ?>
                </td>
                <td class="points"><?php
                    if ($results[$keys[0]]['one'] > $results[$keys[1]]['one']) {
                        echo "<span class='sp-result-won'>" . $results[$keys[0]]['one'] . "</span>" . " - " . $results[$keys[1]]['one'];
                    } else {
                        echo $results[$keys[0]]['one'] . " - " . "<span class='sp-result-won'>" . $results[$keys[1]]['one'] . "</span>";
                    }
                    ?></td>
                <td class="points"><?php
                    if ($results[$keys[0]]['two'] > $results[$keys[1]]['two']) {
                        echo "<span class='sp-result-won'>" . $results[$keys[0]]['two'] . "</span>" . " - " . $results[$keys[1]]['two'];
                    } else {
                        echo $results[$keys[0]]['two'] . " - " . "<span class='sp-result-won'>" . $results[$keys[1]]['two'] . "</span>";
                    }
                    ?></td>
                <td class="points"><?php
                    if ($results[$keys[0]]['three'] > $results[$keys[1]]['three']) {
                        echo "<span class='sp-result-won'>" . $results[$keys[0]]['three'] . "</span>" . " - " . $results[$keys[1]]['three'];
                    } else {
                        echo $results[$keys[0]]['three'] . " - " . "<span class='sp-result-won'>" . $results[$keys[1]]['three'] . "</span>";
                    }
                    ?></td>
                <td class="points"><?php
                    if (isset($results[$keys[0]]['four'])) {
                        if ($results[$keys[0]]['four'] > $results[$keys[1]]['four']) {
                            echo "<span class='sp-result-won'>" . $results[$keys[0]]['four'] . "</span>" . " - " . $results[$keys[1]]['four'];
                        } else {
                            echo $results[$keys[0]]['four'] . " - " . "<span class='sp-result-won'>" . $results[$keys[1]]['four'] . "</span>";
                        }
                    } else {
                        echo "-";
                    }
                    ?></td>
                <td class="points"><?php
                    if (isset($results[$keys[0]]['five'])) {
                        if ($results[$keys[0]]['five'] > $results[$keys[1]]['five']) {
                            echo "<span class='sp-result-won'>" . $results[$keys[0]]['five'] . "</span>" . " - " . $results[$keys[1]]['five'];
                        } else {
                            echo $results[$keys[0]]['five'] . " - " . "<span class='sp-result-won'>" . $results[$keys[1]]['five'] . "</span>";
                        }
                    } else {
                        echo "-";
                    }
                    ?></td>

                <td class="points"><?php
                    if (sizeof($results[$keys[0]]) == 0) {
                        echo "upcoming";
                    } else {
                        $teamresult1 = $results[$keys[0]]['one'] + $results[$keys[0]]['two'] + $results[$keys[0]]['three'] + $results[$keys[0]]['four'] + $results[$keys[0]]['five'];
                        $teamresult2 = $results[$keys[1]]['one'] + $results[$keys[1]]['two'] + $results[$keys[1]]['three'] + $results[$keys[1]]['four'] + $results[$keys[1]]['five'];
                        if ($teamresult1 > $teamresult2) {
                            echo "<span class='sp-result-won'>" . $teamresult1 . "</span>" . " - " . $teamresult2;
                        } else {
                            echo $teamresult1 . " - " . "<span class='sp-result-won'>" . $teamresult2 . "</span>";
                        }
                    }
                    ?></td>

            </tr>

            <?php
        }
        ?>


    </table>
</div>
