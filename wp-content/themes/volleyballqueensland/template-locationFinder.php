<?php
$postcode = isset($_GET['postcode']) ? $_GET['postcode'] : "4000";
$url = "https://maps.googleapis.com/maps/api/geocode/json?libraries=geometry&address=" . $postcode . "+AU&key=AIzaSyCp1vDnOntkD6986M7hIOCu9L6D7IqrmKI";

$data = @file_get_contents($url);
$lat = json_decode($data)->results[0]->geometry->location->lat;
$lon = json_decode($data)->results[0]->geometry->location->lng;
?>
<script>
    var LocsA = [];
    var mapOptions =
            {
                set_center: [<?= $lat ?>, <?= $lon ?>],
                zoom: 10
            };

<?php
/**
 * Template Name: Location Template
 */
$division = [
    'all' => 'All',
    'North Queensland' => 'North Queensland',
    'Central Queensland' => 'Central Queensland',
    'South Queensland' => 'South Queensland',
];
$terms = get_terms(array(
    'taxonomy' => 'sp_venue',
    'hide_empty' => false,
        ));

$args = array(
    'post_type' => 'sp_team',
);

$teams = get_posts($args);
foreach ($terms as $term) {
    $homeCourt = [];
    foreach ($teams as $team) {
        $venue = get_the_terms($team, 'sp_venue')[0];
        $league = get_the_terms($team, 'sp_league')[0];
        $cat = 'sp_league_' . $league->term_id;
        $divisionQ = get_field('division', $cat);

        if (( ($_GET['division'] == $divisionQ)) || !(isset($_GET['division'])) || ($_GET['division'] == 'all')) {

            if ($venue->name == $term->name) {
                $homeCourt['team'] = $team;
                $homeCourt['venue'] = $venue;
                $lat = get_option('taxonomy_' . $term->term_id)['sp_latitude'];
                $long = get_option('taxonomy_' . $term->term_id)['sp_longitude'];
                ?>
                    LocsA.push({
                        lat: <?= $lat ?>,
                        lon: <?= $long ?>,
                        title: '<?= $homeCourt['venue']->name ?>',
                        html: '<h3><?= $homeCourt['venue']->name ?></h3> <p><?= 'Home Court for: ' . $homeCourt['team']->post_title ?></p>',
                        icon: 'http://maps.google.com/mapfiles/marker.png',
                    });
                <?
                break;
            }
        }
    }
}
?>
</script> 

<div class="container">
    <h1>Find Club</h1>
    <form action="/play-learn/find-club/" method="get" class="location-search row">
        <div class="col-3">
            <select class="form-control " name="division">
                <option value="" selected disabled>Select Region</option>
                <?php foreach ($division as $key => $value) {
                    ?>
                    <option value="<?= $key ?>"><?= $value ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="col-3">
            <input type="text" class="form-control   " name="postcode" placeholder="Postcode">
        </div>
        <div class="col-6">
            <input type="submit" class="form-control   skyblue_button" value="search"> 
        </div>
    </form>
    <div id="gmap" style="with:300px;height:400px;"></div>


</div>

<script src="https://maps.google.com/maps/api/js?libraries=geometry&key=AIzaSyCp1vDnOntkD6986M7hIOCu9L6D7IqrmKI">
</script>

<style>
    h1,.location-search,#gmap{
        margin-top:30px;
        margin-bottom:30px;
        text-transform: uppercase;
    }

</style>

<div class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 introbox">
                <h2>welcome to</h2>
                <h3>volleyball queensland</h3>
                <p><?php
                    echo get_field('welcome_text', 'options');
                    ?></p>
                <button class="skyblue_button">read more</button>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 introbox themap">
                <img class="map_image" src="<?= get_field('map_image', 'options') ?>"></img>
            </div>
            <?php
            get_template_part('templates/location-finder');
            ?>
        </div>
    </div>
</div>
