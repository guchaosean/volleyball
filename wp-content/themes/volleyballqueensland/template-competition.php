<?php
/**
 * Template Name: Competition Template
 */
?>

<div class="container">
    <div class="competition content-block">

        <?php get_template_part('templates/content', 'page'); ?>       
        <?php
        //get_template_part('templates/competition-table');
        ?>
    </div>

</div>    


<?php
$competion = get_field('competition');
$cat = 'sp_league_' . $competion;

$backgroundImage = get_field('background-image', $cat);
?>

<style>
    body.page-template-template-competition{
        background:url('<?= $backgroundImage ?>') center top no-repeat fixed;
        background-size: cover;
    }


</style>

