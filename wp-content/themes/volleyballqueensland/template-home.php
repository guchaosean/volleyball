<?php
/**
 * Template Name: Home Template
 */
?>


<?php
$count_posts = wp_count_posts();
if ($count_posts > 0) {
    ?>
    <div class="recent_news">
        <div class="container">
            <div class="row">
                <div class="col-6">
                    <h2>recent news</h2>
                </div>
                <div class="col-6" style="padding-top: 40px;">
                    <a href="/news"><button style="float:right" class="skyblue_button">view all</button></a>
                </div>
            </div>
            <div class="row post">
                <?php
                $args = array(
                    'numberposts' => 3,
                    'offset' => 0,
                    'category' => 0,
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => '',
                    'meta_value' => '',
                    'post_type' => 'post',
                    'post_status' => 'draft, publish, future, pending, private',
                    'suppress_filters' => true
                );

                $recent_posts = wp_get_recent_posts($args, ARRAY_A);
                foreach ($recent_posts as $post) {
                    $img = get_the_post_thumbnail_url($post['ID']);
                    ?>

                    <div class="postslider col-sm-12 col-lg-4">
                        <div class="postthumbnail" style="background-image: url('<?= $img ?>')">
                        </div>
                        <div class="postcontent">
                            <h4><?= $post['post_title'] ?></h4>
                            <p><?= get_the_excerpt($post['ID']) ?></p>

                            <a href="<?= $post['guid'] ?>">read more »</a>
                        </div>

                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<div class="competitions-part">
    <div class="container">
        <h2>competitions</h2>
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <?php
                get_template_part('templates/tournaments-table');
                ?>
                <button class="skyblue_button"><a href="/tournaments">view all</a></button>
            </div>

            <div class="col-md-12 col-lg-6">
                <?php
                // get_template_part('templates/competition-table');
                get_template_part('templates/league-table');
                ?>
                <button class="skyblue_button"><a href="/competitions">view all</a></button>
            </div>
        </div>
    </div>
</div>




<div class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 introbox">
                <h2>welcome to</h2>
                <h3>volleyball queensland</h3>
                <p><?php
                    echo get_field('welcome_text', 'options');
                    ?></p>
                <a href="/about/about-volleyball-qld/"><button class="skyblue_button">read more</button></a>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 introbox themap">
                <img class="map_image" src="<?= get_field('map_image', 'options') ?>"></img>
            </div>
            <?php
            get_template_part('templates/location-finder');
            ?>

        </div>
    </div>
</div>