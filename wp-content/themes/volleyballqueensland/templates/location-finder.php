<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$division = [
    'all' => 'All',
    'North Queensland' => 'North Queensland',
    'Central Queensland' => 'Central Queensland',
    'South Queensland' => 'South Queensland',
];
?>


<div class="col-xl-3 col-lg-6 col-sm-12 col-md-6 introbox">
    <h2>find a club</h2>
    <h3>near me</h3>
    <p><?php
        echo get_field('near_text', 'options');
        ?></p>
    <form action="/find-club/" method="get" class="location-search">
        <select class="form-control" name="division">
            <option value="" selected disabled>Select Region</option>
            <?php foreach ($division as $key => $value) {
                ?>
                <option value="<?= $key ?>"><?= $value ?></option>
                <?php
            }
            ?>


        </select>
        <input type="text" class="form-control" name="postcode" placeholder="Postcode">
        <input type="submit" class="form-control skyblue_button" value="search"> 
    </form>
</div>

<style>
    .location-search input,.location-search select{
        margin-bottom:10px;
        border-radius: 0px;
    }
    .location-search input[type='submit'] {
        width:auto;
    }
</style> 