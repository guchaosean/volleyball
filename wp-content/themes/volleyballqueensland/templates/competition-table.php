<?php
$request_league = $_GET['league'];
$parentsLeague = [];
$childLeague = [];
$terms = get_terms(array(
    'taxonomy' => 'sp_league',
    'hide_empty' => false,
        ));

foreach ($terms as $term) {
    if ($term->parent !== 0) {
        $childLeague[] = $term;
    }
}

foreach ($terms as $term) {
    if ($term->parent == 0) {
        $parentsLeague[] = $term;
    }
}
?>
<div class="dropdown-division">
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Filter by Division
    </button>

    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="?league=All">All</a>
        <?php
        foreach ($parentsLeague as $key => $div) {
            ?>
            <a class="dropdown-item" href="?league=<?= $div->slug ?>"><?= $div->name ?></a>
            <?php
        }
        ?>
    </div>
</div>


<table class="table table-striped table-example table-responsive">
    <tr>      


        <th>league</th>
        <th>division</th>        
        <th></th>
    </tr>
    <?php
    foreach ($childLeague as $competition) {
        $cat = 'sp_league_' . $competition->term_id;
        $parent = $competition->parent;
        $parentObject = get_term($parent);

        $url = get_field('url', $cat);
        if (isset($request_league)) {
            if (($parentObject->slug == $request_league) || ($request_league == 'All')) {
                ?>
                <tr>

                    <td>
                        <?= $parentObject->name ?>
                    </td>

                    <td>
                        <?= $competition->name ?>

                    </td>
                    <td>
                        <span class="table-view"><a href="<?= ($url !== null) ? $url : "#" ?>">view »</a></span>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>

                <td>
                    <?= $parentObject->name ?>
                </td>

                <td>
                    <?= $competition->name ?>

                </td>
                <td>
                    <span class="table-view"><a href="<?= ($url !== null) ? $url : "#" ?>">view »</a></span>
                </td>
            </tr>
            <?php
        }
        ?>

        <?php
    }
    ?>



</table>
<style>
    .dropdown-division{
        float:right;
        margin-bottom:10px;

    }
    .dropdown-division button{
        border-radius:0px;
        background-color: #505050;
        padding:10px;
        text-transform: uppercase;
    }
    .dropdown-division .dropdown-menu{
        background-color: #323a45;
        padding:10px;

    }
    .dropdown-division .dropdown-menu a{
        color:white;
        text-transform: uppercase;
    }
    .dropdown-division .dropdown-menu a:hover{
        background-color:     #4a525f;
    }


    .table-example {
        text-align: center;
        margin-top: 40px;
        margin-bottom: 40px;

    }
    .table-example tr {
        line-height: 3;
    }
    .table-example tr td{
        text-transform: uppercase; 
    }
    .table-example tr th{
        color:white;
        text-transform: uppercase;
        background-color: #505050;
        text-align: center;
    }

    .table-example .dataactive{
        background-color: #32d50d;
    }
    .table-example .data-inactive {
        background-color: #1997c0;
    }
    .table-example .date{
        display:flex;
        position: absolute;
        line-height: 8px;
    }
    .table-example .date div{
        width:33.33%;
    }
    .table-example .day{
        color:white;
        font-size:24px;
    }
    .table-example .seperateor{
        font-size: 48px;
        color:white;
    }
    .table-example .month{
        color:#0e566d;
        font-size:16px;
        text-transform: uppercase;
        font-weight: bold;
    }
    .table-example .table-view{
        text-transform: uppercase;
    }
</style>

