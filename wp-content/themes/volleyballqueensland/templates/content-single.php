

<div class="container">
    <div class="headerimage" style="background-image: url('<?= get_the_post_thumbnail_url() ?>')">

    </div>
    <?php while (have_posts()) : the_post(); ?>
        <div  <?php post_class(); ?>>
            <div class="blogheader">

                <h1 class="entry-title"><?php the_title(); ?></h1>
                <?php get_template_part('templates/entry-meta'); ?>
            </div>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
            <footer>
                <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
            </footer>
            <?php comments_template('/templates/comments.php'); ?>
        </div>
     

    <?php endwhile; ?>
    
    
</div>


