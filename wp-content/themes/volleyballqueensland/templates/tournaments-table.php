<?php
$parentsLeague = [];
$childLeague = [];
$terms = get_terms(array(
    'taxonomy' => 'sp_league',
    'hide_empty' => false,
        ));

foreach ($terms as $term) {
    if ($term->parent !== 0) {
        $childLeague[] = $term;
    }
}

foreach ($terms as $term) {
    if ($term->parent == 0) {
        $parentsLeague[] = $term;
    }
}
?>

<!--
<table class="table table-striped table-example table-responsive">
    <tr>      


        <th>Tournaments</th>
        <th></th>
    </tr>
    <?php
    $filter = [];
    foreach ($parentsLeague as $competition) {
        $type = get_field('type', $competition);
        if ($type == 'tournaments') {
            $filter[] = $competition;
        }
    }
    foreach ($filter as $competition) {
        //$cat = 'sp_league_' . $competition->term_id;
        //$parent = $competition->parent;
        //$parentObject = get_term($parent);
        $logo = get_field('logo', $competition);
        $url = get_field('url', $competition);
        ?>
        <tr>

            <td>
                <img src="<?= $logo ?>" style="max-width:100px;"></img>
                <?= $competition->name ?>
            </td>
            <td>
                <span class="table-view"><a href="<?= ($url !== null) ? $url : "#" ?>">view »</a></span>
            </td>
        </tr>

        <?php
    }
    ?>
</table>-->

<div class="league-table container">
    <div class="row heading">
        <h2>Tournaments</h2>
    </div>
    <?php
    $filter = [];
    foreach ($parentsLeague as $competition) {
        $type = get_field('type', $competition);
        if ($type == 'tournaments') {
            $filter[] = $competition;
        }
    }
    foreach ($filter as $competition) {
        //$cat = 'sp_league_' . $competition->term_id;
        //$parent = $competition->parent;
        //$parentObject = get_term($parent);
        $logo = get_field('logo', $competition);
        $url = get_field('url', $competition);
        ?>
        <div class="row table-row">
            <div class="col-md-12 col-lg-9 team">
                <img src="<?= $logo ?>" style="max-width:100px;"></img>
                <?= $competition->name ?>
            </div>
            <div class="col-md-12 col-lg-3 view">
                <span class="table-view"><a href="<?= ($url !== null) ? $url : "#" ?>">view »</a></span>
            </div>
        </div>



        <?php
    }
    ?>
</div>   
<style>



    .table-example {
        text-align: left;
        margin-top: 40px;
        margin-bottom: 40px;

    }
    .table-example tr {
        line-height: 3;
    }
    .table-example tr td{
        text-transform: uppercase; 
    }
    .table-example tr th{
        color:white;
        text-transform: uppercase;
        background-color: #505050;
        text-align: left;
    }

    .table-example .dataactive{
        background-color: #32d50d;
    }
    .table-example .data-inactive {
        background-color: #1997c0;
    }
    .table-example .date{
        display:flex;
        position: absolute;
        line-height: 8px;
    }
    .table-example .date div{
        width:33.33%;
    }
    .table-example .day{
        color:white;
        font-size:24px;
    }
    .table-example .seperateor{
        font-size: 48px;
        color:white;
    }
    .table-example .month{
        color:#0e566d;
        font-size:16px;
        text-transform: uppercase;
        font-weight: bold;
    }
    .table-example .table-view{
        text-transform: uppercase;
    }
</style>

