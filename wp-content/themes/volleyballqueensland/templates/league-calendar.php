<?php
$tab = isset($_GET['tab']) ? $_GET['tab'] : "current";

$terms = get_terms(array(
    'taxonomy' => 'sp_league',
    'hide_empty' => false,
        ));
foreach ($terms as $term) {
    if ($term->parent !== 0) {
        $childLeague[] = $term;
    }
}
foreach ($childLeague as $competition) {

    $type = get_field('type', $competition);
    $catorgoies = get_field('catergoies', $competition);
    $catorgoies[sizeof($catorgoies)] = null;
    if (($type == 'league') && (in_array($_GET['type'], $catorgoies)) && (in_array($_GET['age'], $catorgoies))) {
        $filters[] = $competition;
    }
}


$queryArr = [ 'relation' => 'AND'];
if ($_GET['season']) {
    array_push($queryArr, [
        'taxonomy' => 'sp_season',
        'field' => 'slug',
        'terms' => $_GET['season'],
    ]);
}
if ((isset($_GET['type'])) || (isset($_GET['age']))) {
    $words = [];
    if (isset($filters)) {
        foreach ($filters as $filter) {
            array_push($words, $filter->slug);
        }
        array_push($queryArr, [
            'taxonomy' => 'sp_league',
            'field' => 'slug',
            'operator' => 'IN',
            'terms' => $words,
        ]);
    }
}

$args = array(
    'post_type' => 'sp_event',
    'order' => 'DESC',
    'posts_per_page' => -1,
    'orderby' => 'date',
    'tax_query' => $queryArr
);
//Get all event based on the query Args
$events = new WP_Query($args);

$seasons = [];

foreach ($events->posts as $event) {
    $season = get_the_terms($event, 'sp_season')[0];
    if (!in_array($season, $seasons)) {
        array_push($seasons, $season);
    }
    if (strtotime($event->post_date) > strtotime('+1 month')) {
        $future[] = $event;
    }
    if ((strtotime($event->post_date) < strtotime('+1 month')) && (strtotime($event->post_date) > strtotime('now'))) {
        $current[] = $event;
    }
    if (strtotime($event->post_date) < strtotime('now')) {
        $completed[] = $event;
    }
}

function renderTable($events, $status) {
    global $_GET;
    if (isset($events)) {
        ?>

        <table class="table table-striped">
            <tr>
                <th>Date</th>
                <th>Round</th>
                <th>League</th>
                <th>Type</th>
                <th>Division</th>
            </tr>
            <?php
            foreach ($events as $event) {
                ?>
                <tr>
                    <td><?= date('M d', strtotime($event->post_date)) ?></td>
                    <td><?= get_field('round', $event->ID); ?></td>
                    <td>
                        <?php
                        $parent = get_the_terms($event, 'sp_league')[0]->parent;
                        $league = get_term_by('term_taxonomy_id', $parent);
                        echo $league->name;
                        ?>
                    </td>
                    <td> 
                        <?php
                        $parent = get_the_terms($event, 'sp_league')[0];
                        $types = get_field('catergoies', $parent);
                        echo implode(' , ', $types);
                        ?>
                    </td>
                    <td>
                        <?php
                        $parent = get_the_terms($event, 'sp_league')[0];
                        $division = get_field('shortfor', $parent);
                        echo $division;
                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table> 
        <?php
    } else {
        ?>
        <div class="alert alert-info" role="alert">
            This is no <?= $status ?> event now.
        </div>
        <?php
    }
}
?>
<div class="button_group">
    <div class="btn-group">
        <a class="all btn <?= sizeof($_GET) > 0 ? "" : "active" ?>" href="/competitions/">All</a>
    </div>
    <div class="btn-group">
        <a class="btn <?= ((isset($_GET['type'])) && ($_GET['type'] == 'Beach')) ? "active" : "" ?>" href="/competitions/?<?= isset($_GET['age']) ? "age=" . $_GET['age'] . "&" : "" ?><?= isset($_GET['season']) ? "season=" . $_GET['season'] . "&" : "" ?>type=Beach&tab=<?= $tab ?>">Beach</a>
        <a class="btn <?= ((isset($_GET['type'])) && ($_GET['type'] == 'Indoor')) ? "active" : "" ?>" href="/competitions/?<?= isset($_GET['age']) ? "age=" . $_GET['age'] . "&" : "" ?><?= isset($_GET['season']) ? "season=" . $_GET['season'] . "&" : "" ?>type=Indoor&tab=<?= $tab ?>">Indoor</a>
    </div>
    <div class="btn-group">
        <a class="btn <?= ((isset($_GET['age'])) && ($_GET['age'] == 'Adult')) ? "active" : "" ?>" href="/competitions/?<?= isset($_GET['type']) ? "type=" . $_GET['type'] . "&" : "" ?><?= isset($_GET['season']) ? "season=" . $_GET['season'] . "&" : "" ?>age=Adult&tab=<?= $tab ?>">Adult</a>
        <a class="btn <?= ((isset($_GET['age'])) && ($_GET['age'] == 'Junior')) ? "active" : "" ?>" href="/competitions/?<?= isset($_GET['type']) ? "type=" . $_GET['type'] . "&" : "" ?><?= isset($_GET['season']) ? "season=" . $_GET['season'] . "&" : "" ?>age=Junior&tab=<?= $tab ?>">Junior</a>
    </div>

    <div class="btn-group">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="season_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?= isset($_GET['season']) ? $_GET['season'] : "Season" ?>
        </button>
        <div class="dropdown-menu" aria-labelledby="season">
            <?php
            foreach ($seasons as $season) {
                ?>
                <button class="dropdown-item" type="button">
                    <a href="/competitions/?<?= isset($_GET['type']) ? "type=" . $_GET['type'] . "&" : "" ?><?= isset($_GET['age']) ? "age=" . $_GET['age'] . "&" : "" ?>season=<?= $season->slug ?>&tab=<?= $tab ?>"><?= $season->name ?></a>
                </button>
            <?php } ?>
        </div>
    </div>
</div>
<ul class="nav nav-tabs" id="calendar" role="tablist">
    <li class="nav-item">
        <a data-ref="upcoming" class="nav-link <?= ($tab == 'upcoming') ? "active" : "" ?>" id="home-tab" data-toggle="tab" href="#upcoming" role="tab" aria-controls="upcoming" aria-selected="true">Upcoming</a>
    </li>
    <li class="nav-item">
        <a data-ref="current" class="nav-link <?= ($tab == 'current') ? "active" : "" ?>" id="profile-tab" data-toggle="tab" href="#current" role="tab" aria-controls="current" aria-selected="false">Current</a>
    </li>
    <li class="nav-item">
        <a data-ref="completed" class="nav-link <?= ($tab == 'completed') ? "active" : "" ?>" id="contact-tab" data-toggle="tab" href="#completed" role="tab" aria-controls="completed" aria-selected="false">Completed</a>
    </li>
</ul>
<div class="tab-content" id="calendar_content">
    <div class="tab-pane fade show <?= ($tab == 'upcoming') ? " show active" : "" ?>" id="upcoming" role="tabpanel" aria-labelledby="home-tab"><?= renderTable($future, 'upcoming') ?></div>
    <div class="tab-pane fade <?= ($tab == 'current') ? " show active" : "" ?>" id="current" role="tabpanel" aria-labelledby="profile-tab"><?= renderTable($current, 'ongoing') ?></div>
    <div class="tab-pane fade <?= ($tab == 'completed') ? " show  active" : "" ?>" id="completed" role="tabpanel" aria-labelledby="contact-tab"><?= renderTable($completed, 'completed') ?></div>
</div>

<?php
        
        
    
 


