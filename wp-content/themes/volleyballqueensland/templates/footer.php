<footer class="content-info">
    <div class="container">
        <div class="row">
            <?php
// check if the repeater field has rows of data
            if (have_rows('footer', 'option')):
                $index = 0;

                // loop through the rows of data
                while (have_rows('footer', 'option')) : the_row();
                    $index++;
                    // display a sub field value
                    ?>
                    <div class="col-lg-6 col-xl-3 col-sm-12">
                        <h2><?= get_sub_field('title'); ?></h2>
                        <?= get_sub_field('content'); ?>
                    </div>
                    <?php
                endwhile;

            endif;
            ?>
        </div>
    </div>    
</footer>
<div class="credit">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">volleyball queensland @ <?= date('Y') ?></div>
            <div class="col-md-6 col-sm-12 designby" >website by <a href="http://clearwebsolutions.com.au/">clearwebsolutions</a></div>
        </div> 
    </div>
</div>    
