<?php

function wp_get_menu_array($current_menu) {
    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID'] = $m->ID;
            $menu[$m->ID]['objectId'] = $m->object_id;
            $menu[$m->ID]['title'] = $m->title;
            $menu[$m->ID]['url'] = $m->url;
            $menu[$m->ID]['children'] = array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID'] = $m->ID;
            $submenu[$m->ID]['objectId'] = $m->object_id;
            $submenu[$m->ID]['title'] = $m->title;
            $submenu[$m->ID]['url'] = $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
}

function checkChildren($array, $parentArray) {
    foreach ($array as $key => $item) {
        if (isset($parentArray[$key])) {
            return true;
        }
    }
    return false;
}

$menuArray = wp_get_menu_array('Menu1');
$thePostID = $post->ID;
?>
<header class="banner-competition">
    <div class="container">
        <div class="row">
            <div class="logo_section col-2">
                <img class="logo" src="<?php the_field('logo', 'option'); ?>"></img>    

            </div>
            <span class=" col-10 tooglemenu-wrap">
                <div class="tooglemenu">
                    <span></span> 
                </div>                    
            </span>
            <div class="menu_section closed col-10">
                <nav>
                    <ul class="navigation">
                        <?php
                        foreach ($menuArray as $item) {
                            if (isset($item['title'])) {
                                if ($item['children'] != null) {
                                    $children = $item['children'];
                                    $fistElement = array_values($children)[0];
                                    if (isset($menuArray[$fistElement['ID']])) {
                                        if (sizeof($children)>0){
                                            $relativeClass="relative";
                                        }else{
                                            $relativeClass="";
                                        }
                                        ?>
                                        <li class="<?= $activeClass." ".$relativeClass ?>">
                                            <?= $item['title'] ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            <div class="megamenu closed row">
                                                <?php
                                                $index = 0;
                                                foreach ($children as $child) {
                                                     $index++;
                                                    ?>
                                                    <div class="submega col-md-4 col-sm-12 <?= ($index == 3) ? "last" : ""?> ">
                                                        <h5><?= $child['title'] ?></h5>
                                                        <ul>
                                                            <?php
                                                            $subchild = $menuArray[$child['ID']]['children'];

                                                            foreach ($subchild as $superchild) {
                                                                ?>
                                                                <a href="<?= $superchild['url'] ?>"><li><?= $superchild['title'] ?></li></a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li>
                                            <?= $item['title'] ?> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            <ul class="sub_menu closed"> 
                                                <?php
                                                foreach ($children as $subitem) {
                                                    ?>
                                                    <a href="<?= $subitem['url'] ?>"><li><?= $subitem['title'] ?></li></a>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <li> <a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li> 
                                    <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                </nav>
            </div> 
        </div> 
    </div>
</header>
<?php
$competion = get_field('competition');
$cat = 'sp_league_' . $competion;
$logo = get_field('logo', $cat);
?>




<?php
$secondMenu = get_field('menu_id');
$menuArray2 = wp_get_menu_array($secondMenu);
?>
<!--Second level menu-->
<div class="container sub-menu-competition">
    <div class="competition-logo" >
        <img   src="<?= $logo ?>">

    </div>
    <div class="menu_section closed second-menu">
        <nav>
            <ul class="navigation">
                <?php
                foreach ($menuArray2 as $item) {
                    if (isset($item['title'])) {
                        if ($item['objectId'] == $thePostID) {
                            $activeClass = "active";
                        } else {
                            $activeClass = "";
                        }
                        if ($item['children'] != null) {
                            $children = $item['children'];
                            $fistElement = array_values($children)[0];
                            if (isset($menuArray2[$fistElement['ID']])) {
                                ?>
                                <li class="<?= $activeClass ?>">
                                    <?= $item['title'] ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <div class="megamenu closed">
                                        <?php
                                        foreach ($children as $child) {
                                            ?>
                                            <div class="submega">
                                                <h5><?= $child['title'] ?></h5>
                                                <ul>
                                                    <?php
                                                    $subchild = $menuArray2[$child['ID']]['children'];

                                                    foreach ($subchild as $superchild) {
                                                        ?>
                                                        <a href="<?= $superchild['url'] ?>"><li><?= $superchild['title'] ?></li></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li class="<?= $activeClass ?>">
                                    <?= $item['title'] ?> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                    <ul class="sub_menu closed"> 
                                        <?php
                                        foreach ($children as $subitem) {
                                            ?>
                                            <a href="<?= $subitem['url'] ?>"><li><?= $subitem['title'] ?></li></a>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                            }
                        } else {
                            ?>
                            <li class="<?= $activeClass ?>"> <a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li> 
                            <?php
                        }
                    }
                }
                ?>
            </ul>
        </nav>
    </div>                         
</div>
