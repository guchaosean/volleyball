<?php

function wp_get_menu_array($current_menu) {
    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID'] = $m->ID;
            $menu[$m->ID]['objectId'] = $m->object_id;
            $menu[$m->ID]['title'] = $m->title;
            $menu[$m->ID]['url'] = $m->url;
            $menu[$m->ID]['children'] = array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID'] = $m->ID;
            $submenu[$m->ID]['objectId'] = $m->object_id;
            $submenu[$m->ID]['title'] = $m->title;
            $submenu[$m->ID]['url'] = $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
}

$menuArray = wp_get_menu_array('Menu1');

function checkChildren($id, $parentArray, $menuArray) {

    foreach ($parentArray['children'] as $key => $item) {
        if (($item['objectId']) == $id) {
            return true;
        }

        if (isset($menuArray[$key])) {

            foreach ($menuArray[$key]['children'] as $keys => $value) {
                if (( $value['objectId'] ) == $id) {
                    return true;
                }
            }
        }
    }
    return false;
}

$thePostID = $post->ID;
?>
<header class="banner">
    <div class="container">
        <div class="row">
            <div class="logo_section col-2">
                <a href="/"><img class="logo" src="<?php the_field('logo', 'option'); ?>"></img></a>
            </div>
            <span class="col-10 tooglemenu-wrap">
                <div class="tooglemenu">
                    <span></span> 
                </div>                    
            </span>
            <div class="menu_section closed col-10">
                <nav>
                    <ul class="navigation">
                        <?php
                        foreach ($menuArray as $item) {
                            if (isset($item['title'])) {

                                if (($item['objectId'] == $thePostID) || ( checkChildren($thePostID, $item, $menuArray) )) {
                                    $activeClass = "active";
                                } else {
                                    $activeClass = "";
                                }
                                if ($item['children'] != null) {
                                    $children = $item['children'];
                                    $fistElement = array_values($children)[0];
                                    if (isset($menuArray[$fistElement['ID']])) {
                                        if (sizeof($children) > 0) {
                                            $relativeClass = "relative";
                                        } else {
                                            $relativeClass = "";
                                        }
                                        ?>
                                        <li class="<?= $activeClass . " " . $relativeClass ?>">
                                            <?= $item['title'] ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            <div class="megamenu closed row">
                                                <?php
                                                $index = 0;
                                                foreach ($children as $child) {
                                                    $index++;
                                                    ?>
                                                    <div class="submega col-md-4 col-sm-12 <?= ($index == 3) ? "last" : "" ?>">
                                                        <h5><?= $child['title'] ?></h5>
                                                        <ul>
                                                            <?php
                                                            $subchild = $menuArray[$child['ID']]['children'];

                                                            foreach ($subchild as $superchild) {
                                                                ?>
                                                                <a href="<?= $superchild['url'] ?>"><li><?= $superchild['title'] ?></li></a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li  class="<?= $activeClass ?>">
                                            <?= $item['title'] ?> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            <ul class="sub_menu closed"> 
                                                <?php
                                                foreach ($children as $subitem) {
                                                    ?>
                                                    <a href="<?= $subitem['url'] ?>"><li><?= $subitem['title'] ?></li></a>
                                                    <?php
                                                }
                                                ?>
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <li  class="<?= $activeClass ?>"> <a href="<?= $item['url'] ?>"><?= $item['title'] ?></a></li> 
                                    <?php
                                }
                            }
                        }
                        ?>
                    </ul>
                </nav>
            </div> 

        </div>    
    </div>
</header>

<?php
if (have_rows('slider')):
    ?>
    <div id="theSLider" class=" slider carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <?php
            $index = 0;
            // loop through the rows of data
            while (have_rows('slider')) : the_row();
                $index++;
                $activeClass = ($index == 1) ? "active" : "";
                //Default slider height
                $sliderHeight = get_field('height');

                $defaultHeight = ($sliderHeight == null) ? '636px' : $sliderHeight . 'px';
                ?>
                <div class="carousel-item <?= $activeClass ?>" style="height:<?= $defaultHeight ?>; background-image:url('<?= get_sub_field('image'); ?>')">
                    <div class="carousel-caption  d-md-block">                   
                        <?php
                        $header_text = get_sub_field('header_text');
                        $content = get_sub_field('content');
                        $button_text = get_sub_field('button_text');
                        $button_url = get_sub_field('button_url');
                        if ($header_text != null) {
                            ?>
                            <h3><?= $header_text ?></h3> 
                            <?php
                        }
                        if ($content != null) {
                            ?>
                            <p><?= $content ?></p> 
                            <?php
                        }
                        if ($button_text != null) {
                            ?>
                            <a target="_blank" href="<?= $button_url ?>"><button class="slider_button"><?= $button_text ?></button> </a>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            // display a sub field value


            endwhile;
            ?>
        </div>
        <?php
        if ($index > 1) {
            ?>
            <a class="carousel-control-prev" href="#theSLider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#theSLider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        <?php } ?>
    </div>
    <?php
endif;
?>

