<?php
/**
 * League Table
 *
 * @author 		ThemeBoy
 * @package 	SportsPress/Templates
 * @version     2.3
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly



$table = new SP_League_Table($id);
$data = $table->data();

$leagu = get_the_terms($id, 'sp_league');
$season = get_the_terms($id, 'sp_season');


$cat = 'sp_league_' . $leagu[0]->term_id;
$sex = get_field('competition_man_or_woman', $cat);

$attributes = $data[0];
unset($data[0]);
?>

<div class="sp-template sp-template-league-table sp-template-league-table-<?= $sex ?> ">

    <h3><?= $leagu[0]->name . " " . $season[0]->name ?></h3>
    <table class="table table-striped sp-league-table ">
        <tr class="doubleheader">
            <th rowspan="2" >Rank</th>
            <th rowspan="2">Teams</th>
            <th colspan="3">Matches</th>
            <th class="result-details-small">Result</th>
            <th colspan="8" class="result-details">Result Details</th>
            <th colspan="3" class="sets">Sets</th>
            <th colspan="3" class="points">Points</th>
        </tr>
        <tr>
            <th class="result-details">Total</th>
            <th class="result-details">Won</th>
            <th class="result-details">Lost</th>
            <th class="result-details-small">T</th>
            <th class="result-details-small">W</th>
            <th class="result-details-small">L</th>

            <th class="result-details">3-0</th>
            <th class="result-details">3-1</th>
            <th class="result-details">3-2</th>
            <th class="result-details">2-3</th>
            <th class="result-details">1-3</th>
            <th class="result-details">0-3</th>
            <th class="result-details">Penalties</th>
            <th  >Points</th>

            <th class="sets">Won</th>
            <th class="sets">Lost</th>
            <th class="sets">Ratio</th>
            <th class="points">Won</th>
            <th class="points">Lost</th>
            <th class="points">Ratio</th>
        </tr>
        <?php
        foreach ($data as $team => $static) {
            ?>
            <tr>
                <td class="sp-standing-rank"><?= $static['pos'] ?></td>
                <td class="sp-standing-name">
                    <?= get_the_post_thumbnail($team, 'sportspress-fit-icon') ?>
                    <div class="clearfix"></div>
                    <?= $static['name'] ?>
                </td>
                <?php
                foreach ($static as $attributes => $value) {
                    if (($attributes !== 'name') && ($attributes !== 'pos')) {
                        ?>
                        <td class="sp-standing-<?= $attributes ?>"><?= $value ?></td> 
                        <?php
                    }
                }
                ?>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

