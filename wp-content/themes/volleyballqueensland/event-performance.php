<?php
/**
 * Event Performance
 *
 * @author 		ThemeBoy
 * @package 	SportsPress/Templates
 * @version     2.3
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

$id = get_the_ID();
$event = new SP_Event($id);
$performance = $event->performance();
$index = 0;
$teamPer[][] = 0;

$attribute = $performance[0];
unset($performance[0]);
foreach ($performance as $per) {
    $index++;
    unset($per[0]);
    foreach ($per as $player => $data) {
        foreach ($data as $key => $value) {
            if (array_key_exists($key, $attribute)) {
                $teamPer[$index][$key] = $teamPer[$index][$key] + $value;
            }
        }
    }
}
unset($teamPer[0]);
?>
<div class="sp-team-performance-new">
    <div class="container">
        <div class="teams">
            <?php
            $teams = array_unique(get_post_meta($event->ID, 'sp_team'));
            ?>
            <div class="row">
                <div class="col-6">
                    <?= get_the_post_thumbnail($teams[0], 'sportspress-fit-icon') ?>
                    <?= get_the_title($teams[0]); ?>
                </div>
                <div class="col-6">
                    <?= get_the_title($teams[1]); ?>
                    <?= get_the_post_thumbnail($teams[1], 'sportspress-fit-icon') ?>

                </div>
            </div>
        </div>
        <?php
        unset($attribute['position']);
        foreach ($attribute as $key => $label) {
            $meanTotal = ($teamPer[1][$key] + $teamPer[2][$key]);
            if ($teamPer[1][$key] >= $teamPer[2][$key]) {
                $style1 = "primary";
                $style2 = "grey";
            } else {
                $style1 = "grey";
                $style2 = "primary";
            }
            ?>
            <div class="row value-bar">
                <div class="col-5">
                    <div class="progress team1-value">
                        <span><?= $teamPer[1][$key] ?></span>
                        <div class="progress-bar progress-bar-info <?= $style1 ?>" role="progressbar" style="width:<?= ($teamPer[1][$key] / $meanTotal) * 100 ?>%; margin-left:<?= (100 - ($teamPer[1][$key] / $meanTotal) * 100) ?>%"></div>
                    </div>
                </div>
                <div class="col-2 metric">
                    <?= $key ?>
                </div>
                <div class="col-5">
                    <div class="progress team2-value">
                        <span><?= $teamPer[2][$key] ?></span>
                        <div class="progress-bar <?= $style2 ?>" role="progressbar" style="width: <?= ($teamPer[2][$key] / $meanTotal) * 100 ?>%"   aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
            <?
        }
        ?>

    </div>

</div>        



