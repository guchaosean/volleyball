<?php
/**
 * Template Name: Find Clud Template
 */
$division = (isset($_GET['division'])) ? $_GET['division'] : "";
$start = (isset($_GET['postcode'])) ? $_GET['postcode'] : "";
  
?>

<div class="container">
    <h2>Find Club</h2>
    <?php
    echo do_shortcode('[wpsl category_selection="' . $division . '"  start_location="' . $start . '" auto_locate="false" ]  ');
    ?>
</div>

<?php

function print_my_inline_script() {
    global $start;
    if (wp_script_is('jquery', 'done')) {
        ?>
        <script type="text/javascript">
           jQuery('#wpsl-search-input').val('<?= $start ?>');
        </script>
        <?php
    }
}

add_action('wp_footer', 'print_my_inline_script');



