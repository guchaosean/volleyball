<?php
/**
 * Template Name: League Calendar Template
 */
$league = get_field('competition');


$calendar = new SP_Calendar($league);
$data = $calendar->data();

$theCalendar = [];
foreach ($data as $event) {
    $round = explode('-', get_field('round', $event->ID))[0];
    $date = date('Y-m-d', strtotime($event->post_date));
    $league = get_the_terms($event, 'sp_league');
    $venue = get_the_terms($event, 'sp_venue')[0]->name;
    $shortfor = explode('-', get_field('shortfor', 'sp_league_' . array_shift($league)->term_id))[1];

    if (isset($theCalendar[$date][$round]['league'])) {
        if (!in_array($shortfor, $theCalendar[$date][$round]['league'])) {
            array_push($theCalendar[$date][$round]['league'], $shortfor);
        }
        if (!in_array($venue, $theCalendar[$date][$round]['venue'])) {
            array_push($theCalendar[$date][$round]['venue'], $venue);
        }
    } else {
        $theCalendar[$date][$round]['league'] = [];
        $theCalendar[$date][$round]['venue'] = [];
        array_push($theCalendar[$date][$round]['league'], $shortfor);
        array_push($theCalendar[$date][$round]['venue'], $venue);
    }
}

$customizedSession = [];
if (have_rows('customized_session')):

    // loop through the rows of data
    while (have_rows('customized_session')) : the_row();

        // display a sub field value
        $startDate = date('Y-m-d', strtotime(get_sub_field('date_start')));
        $endDate = date('Y-m-d', strtotime(get_sub_field('date_end')));
        $description = get_sub_field('event_description');
        $location = get_sub_field('location');
        if (!isset($customizedSession[$startDate]['customized'])) {
            $customizedSession[$startDate]['customized'][0]['end'] = $endDate;
            $customizedSession[$startDate]['customized'][0]['event'] = $description;
            $customizedSession[$startDate]['customized'][0]['location'] = $location;
        } else {
            $data = [
                'end' => $endDate,
                'event' => $description,
                'location' => $location
            ];
            array_push($customizedSession[$startDate]['customized'], $data);
        }

    endwhile;


endif;

$finalCalendar = array_merge_recursive($theCalendar, $customizedSession);
ksort($finalCalendar);


foreach ($finalCalendar as $key => $data) {
    if (!isset($table[date('m', strtotime($key))])) {
        $table[date('m', strtotime($key))][0] = [$key => $data];
    } else {
        array_push($table[date('m', strtotime($key))], [$key => $data]);
    }
}
?>
<div class="container">
    <div class="competition content-block">
        <h1>Calendar</h1>
        <table class="table table-striped">
            <tr>
                <th class="result-details">MONTH</th> 
                <th>DATE</th>
                <th>EVENT</th>
                <th>LOCATION</th>
            </tr>
            <?php
            foreach ($table as $month => $data) {
                $startofMonth = true;
                foreach ($data as $key => $events) {
                    foreach ($events as $startdate => $blocks) {
                        foreach ($blocks as $round => $event) {
                            if ($round == 'customized') {
                                foreach ($event as $key => $uu) {
                                    $monthTag = ($startofMonth) ? DateTime::createFromFormat('!m', $month)->format('F') : "";
                                    ?>
                                    <tr>
                                        <td class="result-details"><?= $monthTag ?></td>
                                        <td class="result-details"><?= date('m-d', strtotime($startdate)) . " to " . date('m-d', strtotime($uu['end'])) ?></td>
                                        <td class="result-details-small">
                                            <p><?= date('d/m', strtotime($startdate)) ?></p>
                                            <p><?= " to " ?></p>
                                            <p><?= date('d/m', strtotime($uu['end'])) ?></p>

                                        </td>
                                        <td><?= $uu['event'] ?> </td>
                                        <td><?= $uu['location'] ?> </td>
                                    </tr>
                                    <?php
                                }
                                $startofMonth = false;
                            } else {
                                $monthTag = ($startofMonth) ? DateTime::createFromFormat('!m', $month)->format('F') : "";
                                switch ($round) {
                                    case "pre":
                                        $roundText = 'PRELIMINARY FINAL (' . implode(',', $event['league']) . ")";
                                        break;
                                    case "semi":
                                        $roundText = 'SEMI FINAL (' . implode(',', $event['league']) . ")";
                                        break;
                                    case "final":
                                        $roundText = 'GRAND FINAL (' . implode(',', $event['league']) . ")";
                                        break;
                                    default:
                                        $roundText = 'Round ' . $round . " (" . implode(',', $event['league']) . ")";
                                        break;
                                }
                                ?>
                                <tr>
                                    <td  class="result-details"><?= $monthTag ?></td>
                                    <td  class="result-details"><?= date('d', strtotime($startdate)) ?></td>
                                    <td  class="result-details-small"><?= date('d/m', strtotime($startdate)) ?></td>
                                    <td><?= $roundText ?></td>
                                    <td><?= implode(' , ', $event['venue']) ?></td>

                                </tr>
                                <?php
                                $startofMonth = false;
                            }
                        }
                    }
                }
            }
            ?>
        </table>
    </div>
</div>


<?php
$competion = get_field('competition');
$cat = 'sp_league_' . $competion;

$backgroundImage = get_field('background-image', $cat);
?>

<style>
    body.page-template-template-league-calendar{
        background:url('<?= $backgroundImage ?>') center top no-repeat fixed;
        background-size: cover;
    }
    .competition .table {
        text-align:center;
    }
    .competition .table th{
        text-align:center;
    }

</style>