<?php

/*
  Plugin Name: Resource shortcode
  Version: 1.0
  Author: Chao Gu
  License: MIT
  Description: Allow Resource to be display on the page using simple shortcode [resource name="resource_name"]
 */

function resource_shortcode($atts) {

    if (isset($atts['name'])) {
        $name = $atts['name'];
    } else {
        $name = "";
    }

    $output = $output . '[su_spoiler title="' . $atts['name'] . '" open="yes" style="default" icon="plus" anchor="" class=""]';
    $output = $output . ' <div class="resource-body">';
    $query_images_args = array(
        'post_type' => 'attachment',
        'post_status' => 'inherit',
        'tax_query' => array(
            array(
                'taxonomy' => 'category_media',
                'field' => 'name',
                'terms' => $name,
            ),
        ),
    );
    $query_images = new WP_Query($query_images_args);
    
    foreach ($query_images->posts as $post) {

        $name = $post->post_title;
        $url = $post->guid;


        $output = $output . '<p><a href="' . $url . '">' . $name . '</a></p>';
    }

    $output = $output . '</div>';
    $output = $output . '[/su_spoiler]';

    return do_shortcode($output);
}

add_shortcode('resource', 'resource_shortcode');
?>